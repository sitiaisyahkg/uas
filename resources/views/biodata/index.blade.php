@include('base.header')
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>ThemeKit - Admin Template</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />

        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">
        
        <link rel="stylesheet" href="{{url('assets/plugins/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/fontawesome-free/css/all.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/icon-kit/dist/css/iconkit.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/ionicons/dist/css/ionicons.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/jvectormap/jquery-jvectormap.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/weather-icons/css/weather-icons.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/c3/c3.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/owl.carousel/dist/assets/owl.carousel.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/plugins/owl.carousel/dist/assets/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{url('assets/dist/css/theme.min.css')}}">
        <script src="src/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="wrapper">
            <header class="header-top" header-theme="light">
                <div class="container-fluid">
                    <div class="d-flex justify-content-between">
                        <div class="top-menu d-flex align-items-center">
                            <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
                            <div class="header-search">
                                <div class="input-group">
                                    <span class="input-group-addon search-close"><i class="ik ik-x"></i></span>
                                    <input type="text" class="form-control">
                                    <span class="input-group-addon search-btn"><i class="ik ik-search"></i></span>
                                </div>
                            </div>
                            <button type="button" id="navbar-fullscreen" class="nav-link"><i class="ik ik-maximize"></i></button>
                        </div>
                        
                    </div>
                </div>
            </header>

            <div class="page-wrap">
                <div class="app-sidebar colored">
                    <div class="sidebar-header">
                        <a class="header-brand" href="index.html">
                            
                            <span class="text">ThemeKit</span>
                        </a>
                        <button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
                        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
                    </div>
                    
                    <div class="sidebar-content">
                        <div class="nav-container">
                            <nav id="main-menu-navigation" class="navigation-main">
                                <div class="nav-lavel">Navigation</div>
                                <div class="nav-item">
                                    <a href="jenjang"><i class="ik ik-bar-chart-2"></i><span>Jenjang Pendidikan</span></a>
                                </div>
                                <div class="nav-item">
                                    <a href="biodata"><i class="ik ik-bar-chart-2"></i><span>Profile</span></a>
                                </div>
                                <div class="nav-item">
                                    <a href="acara"><i class="ik ik-bar-chart-2"></i><span>Acara</span></a>
                                </div>
                                <div class="nav-item">
                                    <a href="pekerjaan"><i class="ik ik-bar-chart-2"></i><span>Pekerjaan</span></a>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="main-content">
                    <div class="container-fluid">
                        <div class="page-header">
                            <div class="row align-items-end">
                                <div class="col-lg-8">
                                    <div class="page-header-title">
                                        <i class="ik ik-edit bg-blue"></i>
                                        <div class="d-inline">
                                            <h5>Components</h5>
                                            <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="../index.html"><i class="ik ik-home"></i></a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#">Forms</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Components</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>

                        <section class="content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header"><h3>Tambah Biodata</h3></div>
                                    <div class="card-body">
                                        <form class="forms-sample" role="form" action="/biodata" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <div class="box-body">
                                                {{-- get session flash --}}
                                                    @if(Session::has('message'))
                                                      <h4><strong>{{session::get('message')}}</strong></h4>
                                                    @endif
                                  
                                                  {{-- get validation --}}
                                                  @if (count($errors) > 0)
                                  
                                                <div class="alert alert-danger">
                                                    <ul>
                                                      @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                      @endforeach
                                                    </ul>
                                                </div>
                                                     @endif

                                            <div class="form-group">
                                                <label for="exampleInputUsername1">Nama Lengkap</label>
                                                <input type="text" class="form-control" placeholder="Masukkan Nama Lengkap">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputUsername1">Alamat Lengkap</label>
                                                <input type="text" class="form-control" placeholder="Masukkan Alamat Lengkap">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputUsername1">Nomor Telpon</label>
                                                <input type="text" class="form-control" placeholder="Masukkan Telpon-mu">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputUsername1">Email</label>
                                                <input type="text" class="form-control" placeholder="Masukkan Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputUsername1">Tempat dan Tanggal Lahir</label>
                                                <input type="text" class="form-control" placeholder="Masukkan Tempat dan Tangal Lahir">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputUsername1">Agama</label>
                                                <input type="text" class="form-control" placeholder="Masukkan Agamamu">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputUsername1">Nama Sekolah/Universitas</label>
                                                <input type="text" class="form-control" placeholder="Masukkan Nama Sekolah-mu">
                                            </div>
                                            <div class="form-group">
                                                <label>Pilih Jenjang Pendidikan</label>
                                                {{-- <input type="text" class="form-control" name="kategori_id" placeholder="Enter Category..."> --}}
                                                <select class="form-control" name="jenjang_id">
                                                  @foreach($Jenjang as $jenjang)
                                                    <option value="{{ $jenjang->id }}">{{ $jenjang->jenjang_terakhir }}</option>
                                                  @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Image</label>
                                                <input type="file" name="image">
                                              </div>
                                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                          </form>
                                    </div>
                                </div>
                            </div>
                        

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header"><h3>Data Profile</h3></div>
                                    <div class="card-body">
                                        <table id="data_table" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Nama</th>
                                                    <th>Alamat</th>
                                                    <th>Nomor Tlpn</th>
                                                    <th>Email</th>
                                                    <th>Action</th>
                                                    <th class="nosort">&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($Biodata as $item)
                                                <tr>
                                                    <td>{{ $item->id }}</td>
                                                    <td>{{ $item->nama_lengkap }}</td>
                                                    <td>{{ $item->alamat }}</td>
                                                    <td>{{ $item->nomor_telepon }}</td>
                                                    <td>{{ $item->email }}</td>
                                                    <td>
                                                        <div class="table-actions">
                                                            <a href="/biodata/{{ $item->id }}"><i class="ik ik-eye"></i></a>
                                                            <a href="/biodata/{{$item->id}}/edit"><i class="ik ik-edit-2"></i></a>
                                                            <a href="/biodata/{{$item->id}}/delete"><i class="ik ik-trash-2"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header"><h3>Data Profile</h3></div>
                                    <div class="card-body">
                                        <table id="data_table" class="table">
                                            <thead>
                                                <tr>
                                                    <th>Tempat/Tgl Lahir</th>
                                                    <th>Agama</th>
                                                    <th>Sekolah/Universitas</th>
                                                    <th>Pendidikan</th>
                                                    <th>Image</th>
                                                    <th>Action</th>
                                                    <th class="nosort">&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($Biodata as $item)
                                                <tr>
                                                    <td>{{ $item->tempat_tanggal_lahir }}</td>
                                                    <td>{{ $item->agama }}</td>
                                                    <td>{{ $item->sekolah_atau_universitas }}</td>
                                                    <td>{{ $item->jenjang_id }}</td>
                                                    <td>{{ $item->image }}</td>
                                                    <td>
                                                        <div class="table-actions">
                                                            <a href="/biodata/{{ $item->id }}"><i class="ik ik-eye"></i></a>
                                                            <a href="/biodata/{{$item->id}}/edit"><i class="ik ik-edit-2"></i></a>
                                                            <a href="/biodata/{{$item->id}}/delete"><i class="ik ik-trash-2"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </section>
                    </div>
                </div>
            </div>
@include('base.footer')