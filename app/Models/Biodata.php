<?php

namespace App\Models;

use App\Models\Jenjang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Biodata extends Model
{
    use SoftDeletes;
    
    protected $table = "biodata";

    public function jenjang(){
        return $this->belongsTo('App\Models\Jenjang');
    }
}
