<?php

namespace App\Models;

use App\Models\Biodata;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jenjang extends Model
{
    use SoftDeletes;
    
    protected $table = "jenjang";

    public function biodata(){
    	return $this->hasMany('App\Models\Biodata');
    }
}
