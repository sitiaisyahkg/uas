<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Acara extends Model
{
    protected $table = 'acara';

    use softDeletes;

    public function acara()
    {
    	return $this->hasMany(Acara::class, 'nama_pelatihan', 'id');
    }
}
