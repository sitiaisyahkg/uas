<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    protected $table = 'pekerjaan';

    use softDeletes;

    public function pekerjaan()
    {
    	return $this->hasMany(Acara::class, 'nama_pelatihan', 'id');
    }
}
