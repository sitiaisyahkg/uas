<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Biodata;
use App\Models\Jenjang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BiodataController extends Controller
{
    
    public function index()
    {
      $Jenjang = Jenjang::all();
      $Biodata = Biodata::all();

      return view('biodata.index', compact('Biodata', 'Jenjang'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_lengkap' => 'required',
            'alamat' => 'required',
            'nomor_telepon' => 'required',
            'email' => 'required',
            'tempat_tanggal_lahir' => 'required',
            'agama' => 'required',
            'sekolah_atau_universitas' => 'required',
            'jenjang_id' => 'required',
            'image' => 'required | image | mimes:jpg,jpeg,png,gif'
        ]);

        try{
            $image = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('image'), $image);

            $Biodata = new Biodata();
            $Biodata->jenjang_id = $request->jenjang_id;

            $Biodata->nama_lengkap = $request->nama_lengkap;
            $Biodata->alamat = $request->alamat;
            $Biodata->nomor_telepon = $request->nomor_telepon;
            $Biodata->email = $request->email;
            $Biodata->tempat_tanggal_lahir = $request->tempat_tanggal_lahir;
            $Biodata->agama = $request->agama;
            $Biodata->sekolah_atau_universitas = $request->sekolah_atau_universitas;
            $Biodata->image = $image;

            $Biodata->save();

            Session::flash('message', 'Data berhasil disimpan');
            return redirect()->back();
            } catch (Exception $e) {
            Session::flash('message', 'Data tidak berhasil disimpan');
            return redirect()->back();
        }      
    }

  public function detail($id)
  {

    $Biodata = Biodata::find($id);

    return view('biodata.detail', compact('Biodata'));
  }

  public function delete($id)
  {
    $Biodata = Biodata::find($id);

    $Biodata->delete();

    Session::flash('message', 'Berhasil menghapus');

    return redirect()->back();
  }

  public function edit($id)
  {
    $Biodata = Biodata::find($id);
     
    return view('biodata.edit', compact('Biodata'));
  }

  public function update(Request $request, $id)
  {
    $Biodata = Biodata::find($id);
    $Biodata->jenjang_id = $request->jenjang_id;

    $Biodata->nama_lengkap = $request->nama_lengkap;
    $Biodata->alamat = $request->alamat;
    $Biodata->nomor_telepon = $request->nomor_telepon;
    $Biodata->email = $request->email;
    $Biodata->tempat_tanggal_lahir = $request->tempat_tanggal_lahir;
    $Biodata->agama = $request->agama;
    $Biodata->sekolah_atau_universitas = $request->sekolah_atau_universitas;
    $Biodata->image = $image;

    $Biodata->save();
    Session::flash('message',' Berhasil Update');

    return redirect()->back();
  }
}
