<?php

namespace App\Http\Controllers\API\V1;

use Exception;
use App\Models\Acara;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AcaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Acara = Acara::all();

            $response = $Acara;
            $code = 200;
        }catch (Exception $e){
            $code = 500;
            $response = $e -> getMessage();
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_pelatihan' => 'required',
            'penyelenggara' => 'required',
            'tahun' => 'required',
            'tempat' => 'required'
        ]);

        try {
            $Acara = new Acara();

            $Acara->nama_pelatihan = $request->nama_pelatihan;
            $Acara->penyelenggara = $request->penyelenggara;
            $Acara->tahun = $request->tahun;
            $Acara->tempat = $request->tempat;

            $Acara->save();
            $code=200;
            $response=$Acara;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Acara = Acara::findOrFail($id);

            $code = 200;
            $response = $Acara;
        }catch (Exception $e){
            if ($e instanceof ModelNotFoundException){
                $code = 400;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama_pelatihan' => 'required',
            'penyelenggara' => 'required',
            'tahun' => 'required',
            'tempat' => 'required'
        ]);

        try {
            $Acara = new Acara();

            $Acara->nama_pelatihan = $request->nama_pelatihan;
            $Acara->penyelenggara = $request->penyelenggara;
            $Acara->tahun = $request->tahun;
            $Acara->tempat = $request->tempat;

            $Acara->save();
            $code=200;
            $response=$Acara;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Acara = Acara::find($id);
            $Acara->delete();

            $code = 200;
            $response = $Acara;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code, $response);
    }
}
