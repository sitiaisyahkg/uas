<?php

namespace App\Http\Controllers\API\V1;

use Exception;
use App\Models\Jenjang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class JenjangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Jenjang = Jenjang::all();

            $response = $Jenjang;
            $code = 200;
        }catch (Exception $e){
            $code = 500;
            $response = $e -> getMessage();
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'jenjang_terakhir' => 'required'
        ]);

        try {
            $Jenjang = new Jenjang();

            $Jenjang->jenjang_terakhir = $request->jenjang_terakhir;

            $Jenjang->save();
            $code=200;
            $response=$Jenjang;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        try {
            $Jenjang = Jenjang::findOrFail($id);

            $code = 200;
            $response = $Jenjang;
        }catch (Exception $e){
            if ($e instanceof ModelNotFoundException){
                $code = 400;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'jenjang_terakhir' => 'required'
        ]);

        try {
            $Jenjang = new Jenjang();

            $Jenjang->jenjang_terakhir = $request->jenjang_terakhir;

            $Jenjang->save();
            $code=200;
            $response=$Jenjang;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Jenjang = Jenjang::find($id);
            $Jenjang->delete();

            $code = 200;
            $response = $Jenjang;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code, $response);
    }
}
