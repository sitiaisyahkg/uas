<?php

namespace App\Http\Controllers\API\V1;

use Exception;
use App\Models\Pekerjaan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PekerjaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Pekerjaan = Pekerjaan::all();

            $response = $Pekerjaan;
            $code = 200;
        }catch (Exception $e){
            $code = 500;
            $response = $e -> getMessage();
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'perusahaan' => 'required',
            'alamat' => 'required',
            'jurusan' => 'required',
            'jabatan' => 'required',
            'tahun_mulai' => 'required',
            'tahun_akhir' => 'required'
        ]);

        try {
            $Pekerjaan = new Pekerjaan();

            $Pekerjaan->perusahaan = $request->perusahaan;
            $Pekerjaan->alamat = $request->alamat;
            $Pekerjaan->jurusan = $request->jurusan;
            $Pekerjaan->jabatan = $request->jabatan;
            $Pekerjaan->tahun_mulai = $request->tahun_mulai;
            $Pekerjaan->tahun_akhir = $request->tahun_akhir;

            $Pekerjaan->save();
            $code=200;
            $response=$Pekerjaan;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Pekerjaan = Pekerjaan::findOrFail($id);

            $code = 200;
            $response = $Pekerjaan;
        }catch (Exception $e){
            if ($e instanceof ModelNotFoundException){
                $code = 400;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'perusahaan' => 'required',
            'alamat' => 'required',
            'jurusan' => 'required',
            'jabatan' => 'required',
            'tahun_mulai' => 'required',
            'tahun_akhir' => 'required'
        ]);

        try {
            $Pekerjaan = new Pekerjaan();

            $Pekerjaan->perusahaan = $request->perusahaan;
            $Pekerjaan->alamat = $request->alamat;
            $Pekerjaan->jurusan = $request->jurusan;
            $Pekerjaan->jabatan = $request->jabatan;
            $Pekerjaan->tahun_mulai = $request->tahun_mulai;
            $Pekerjaan->tahun_akhir = $request->tahun_akhir;

            $Pekerjaan->save();
            $code=200;
            $response=$Pekerjaan;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Pekerjaan = Pekerjaan::find($id);
            $Pekerjaan->delete();

            $code = 200;
            $response = $Pekerjaan;
        }catch (\Exception $e){
            $code = 500;
            $response = $e->getMessage();
        }

        return apiResponseBuilder($code, $response);
    }
}
