<?php

namespace App\Http\Controllers\API\V1;

use Exception;
use App\Models\Biodata;
use App\Models\Jenjang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BiodataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $Biodata = Biodata::all();

            $response = $Biodata;
            $code = 200;
        }catch (Exception $e){
            $code = 500;
            $response = $e -> getMessage();
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nama_lengkap' => 'required',
            'alamat' => 'required',
            'nomor_telepon' => 'required',
            'email' => 'required',
            'tempat_tanggal_lahir' => 'required',
            'agama' => 'required',
            'sekolah_atau_universitas' => 'required',
            'jenjang_id' => 'required',
            'image' => 'required | image | mimes:jpg,jpeg,png,gif'

        ]);

        try {
            $image = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('image'), $image);

            $Biodata = new Biodata();
            $Biodata->jenjang_id = $request->jenjang_id;

            $Biodata->nama_lengkap = $request->nama_lengkap;
            $Biodata->alamat = $request->alamat;
            $Biodata->nomor_telepon = $request->nomor_telepon;
            $Biodata->email = $request->email;
            $Biodata->tempat_tanggal_lahir = $request->tempat_tanggal_lahir;
            $Biodata->agama = $request->agama;
            $Biodata->sekolah_atau_universitas = $request->sekolah_atau_universitas;
            $Biodata->image = $image;

            $Jenjang = Jenjang::where('id', $request->jenjang_id)->first();

            //jenjang
            if($Jenjang == null){
                return apiResponseValidationFails('Jenjang Not Found');
            }

            $Biodata->save();
            $code=200;
            $response=$Biodata;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $Biodata = Biodata::findOrFail($id);

            $code = 200;
            $response = $Biodata;
        }catch (Exception $e){
            if ($e instanceof ModelNotFoundException){
                $code = 400;
                $response = 'inputkan sesuai id';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nama_lengkap' => 'required',
            'alamat' => 'required',
            'nomor_telepon' => 'required',
            'email' => 'required',
            'tempat_tanggal_lahir' => 'required',
            'agama' => 'required',
            'sekolah_atau_universitas' => 'required',
            'jenjang_id' => 'required',
            'image' => 'required'

        ]);

        try {

            $Biodata = new Biodata();
            $Biodata->jenjang_id = $request->jenjang_id;

            $Biodata->nama_lengkap = $request->nama_lengkap;
            $Biodata->alamat = $request->alamat;
            $Biodata->nomor_telepon = $request->nomor_telepon;
            $Biodata->email = $request->email;
            $Biodata->tempat_tanggal_lahir = $request->tempat_tanggal_lahir;
            $Biodata->agama = $request->agama;
            $Biodata->sekolah_atau_universitas = $request->sekolah_atau_universitas;
            $Biodata->image = $request->$image;

            $Jenjang = Jenjang::where('id', $request->jenjang_id)->first();

            //jenjang
            if($Jenjang == null){
                return apiResponseValidationFails('Jenjang Not Found');
            }

            $Biodata->save();
            $code=200;
            $response=$Biodata;

            } catch (Exception $e) {
                if($e instanceof ValidationException){
                    $code = 400;
                    $response = 'tidak ada data';
                }else{
                    $code= 500;
                    $response =$e->getMessage();
                }
            }
            return apiResponseBuilder($code, $response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $Biodata = Biodata::find($id);
            $Biodata->delete();

            $code = 200;
            $response = $Biodata;
        }catch (Exception $e){
            $code = 500;
            $response=$e->getMessage();
        }

        return apiResponseBuilder($code,$response);
    }
}
