<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Jenjang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class JenjangController extends Controller
{
    
    public function index()
    {
      $Jenjang = Jenjang::all();

      return view('jenjang.index', compact('Jenjang'));
    }

    public function store(Request $request)
    {
      $this->validate($request, [
            'jenjang_terakhir' => 'required'

        ]);

        try{

            $Jenjang = new Jenjang();

            $Jenjang->jenjang_terakhir = $request->jenjang_terakhir;

            $Jenjang->save();
            $code=200;
            $response=$Jenjang;

         Session::flash('message', 'Data berhasil disimpan');
         return redirect()->back();
     } catch (Exception $e) {
       Session::flash('message', 'Data tidak berhasil disimpan');
       return redirect()->back();
      }      
  }

  public function detail($id)
  {

    $Jenjang = Jenjang::find($id);

    return view('jenjang.detail', compact('Jenjang'));
  }

  public function delete($id)
  {
    $Jenjang = Jenjang::find($id);

    $Jenjang->delete();

    Session::flash('message', 'Berhasil menghapus');

    return redirect()->back();
  }

  public function edit($id)
  {
    $Jenjang = Jenjang::find($id);
     
    return view('jenjang.edit', compact('Jenjang'));
  }

  public function update(Request $request, $id)
  {
    $Jenjang = Jenjang::find($id);
    $Jenjang->jenjang_terakhir = $request->jenjang_terakhir;
    
    $Jenjang->save();
    Session::flash('message',' Berhasil Update');

    return redirect()->back();
  }
}
