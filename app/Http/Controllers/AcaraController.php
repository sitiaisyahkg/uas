<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Acara;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AcaraController extends Controller
{
    
    public function index()
    {
      $Acara = Acara::all();

      return view('acara.index', compact('Acara'));
    }

    public function store(Request $request)
    {
      $this->validate($request, [
            'nama_pelatihan' => 'required',
            'penyelenggara' => 'required',
            'tahun' => 'required',
            'tempat' => 'required'

        ]);

        try{

            $Acara = new Acara();

            $Acara->nama_pelatihan = $request->nama_pelatihan;
            $Acara->penyelenggara = $request->penyelenggara;
            $Acara->tahun = $request->tahun;
            $Acara->tempat = $request->tempat;

            $Acara->save();
            $code=200;
            $response=$Acara;

         Session::flash('message', 'Data berhasil disimpan');
         return redirect()->back();
     } catch (Exception $e) {
       Session::flash('message', 'Data tidak berhasil disimpan');
       return redirect()->back();
      }      
  }

  public function detail($id)
  {

    $Acara = Acara::find($id);

    return view('acara.detail', compact('Acara'));
  }

  public function delete($id)
  {
    $Acara = Acara::find($id);

    $Acara->delete();

    Session::flash('message', 'Berhasil menghapus');

    return redirect()->back();
  }

  public function edit($id)
  {
    $Acara = Acara::find($id);
     
    return view('acara.edit', compact('Acara'));
  }

  public function update(Request $request, $id)
  {
    $Acara = Acara::find($id);
    $Acara->nama_pelatihan = $request->nama_pelatihan;
    $Acara->penyelenggara = $request->penyelenggara;
    $Acara->tahun = $request->tahun;
    $Acara->tempat = $request->tempat;

    $Acara->save();
    Session::flash('message',' Berhasil Update');

    return redirect()->back();
  }
}
