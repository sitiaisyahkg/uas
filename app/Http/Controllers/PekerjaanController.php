<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Pekerjaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PekerjaanController extends Controller
{
    
    public function index()
    {
      $Pekerjaan = Pekerjaan::all();

      return view('pekerjaan.index', compact('Pekerjaan'));
    }

    public function store(Request $request)
    {
      $this->validate($request, [
            'perusahaan' => 'required',
            'alamat' => 'required',
            'jurusan' => 'required',
            'jabatan' => 'required',
            'tahun_mulai' => 'required',
            'tahun_akhir' => 'required'

        ]);

        try{

            $Pekerjaan = new Pekerjaan();

            $Pekerjaan->perusahaan = $request->perusahaan;
            $Pekerjaan->alamat = $request->alamat;
            $Pekerjaan->jurusan = $request->jurusan;
            $Pekerjaan->jabatan = $request->jabatan;
            $Pekerjaan->tahun_mulai = $request->tahun_mulai;
            $Pekerjaan->tahun_akhir = $request->tahun_akhir;

            $Pekerjaan->save();
            $code=200;
            $response=$Pekerjaan;

         Session::flash('message', 'Data berhasil disimpan');
         return redirect()->back();
     } catch (Exception $e) {
       Session::flash('message', 'Data tidak berhasil disimpan');
       return redirect()->back();
      }      
  }

  public function detail($id)
  {

    $Pekerjaan = Pekerjaan::find($id);

    return view('pekerjaan.detail', compact('Pekerjaan'));
  }

  public function delete($id)
  {
    $Pekerjaan = Pekerjaan::find($id);

    $Pekerjaan->delete();

    Session::flash('message', 'Berhasil menghapus');

    return redirect()->back();
  }

  public function edit($id)
  {
    $Pekerjaan = Pekerjaan::find($id);
     
    return view('pekerjaan.edit', compact('Pekerjaan'));
  }

  public function update(Request $request, $id)
  {
    $Pekerjaan = Pekerjaan::find($id);
    $Pekerjaan->perusahaan = $request->perusahaan;
    $Pekerjaan->alamat = $request->alamat;
    $Pekerjaan->jurusan = $request->jurusan;
    $Pekerjaan->jabatan = $request->jabatan;
    $Pekerjaan->tahun_mulai = $request->tahun_mulai;
    $Pekerjaan->tahun_akhir = $request->tahun_akhir;

    $Pekerjaan->save();
    Session::flash('message',' Berhasil Update');

    return redirect()->back();
  }
}
