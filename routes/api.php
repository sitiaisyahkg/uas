<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('jenjang', 'API\V1\JenjangController');
Route::apiResource('biodata', 'API\V1\BiodataController');
Route::apiResource('acara', 'API\V1\AcaraController');
Route::apiResource('pekerjaan', 'API\V1\PekerjaanController');

//biodata
// Route::post('/biodata', "API\V1\BiodataController@store");
// Route::get('/biodata', "API\V1\BiodataController@index");
// Route::get('/biodata/{id}', "API\V1\BiodataController@show");
// Route::put('/biodata/{id}', "API\V1\BiodataController@update");
// Route::delete('/biodata/{id}', "API\V1\BiodataController@destroy");