<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template');
});

//jenjang
Route::get('jenjang', 'JenjangController@index');
Route::post('jenjang', 'JenjangController@store');
Route::get('jenjang/{id}', 'JenjangController@detail');
Route::get('jenjang/{id}/delete', 'JenjangController@delete');
Route::post('jenjang/{id}/update', 'JenjangController@update');
Route::get('jenjang/{id}/edit', 'JenjangController@edit');

//profile/bio
Route::get('biodata', 'BiodataController@index');
Route::post('biodata', 'BiodataController@store');
Route::get('biodata/{id}', 'BiodataController@detail');
Route::get('biodata/{id}/delete', 'BiodataController@delete');
Route::post('biodata/{id}/update', 'BiodataController@update');
Route::get('biodata/{id}/edit', 'BiodataController@edit');

//acara
Route::get('acara', 'AcaraController@index');
Route::post('acara', 'AcaraController@store');
Route::get('acara/{id}', 'AcaraController@detail');
Route::get('acara/{id}/delete', 'AcaraController@delete');
Route::post('acara/{id}/update', 'AcaraController@update');
Route::get('acara/{id}/edit', 'AcaraController@edit');

//pekerjaan
Route::get('pekerjaan', 'PekerjaanController@index');
Route::post('pekerjaan', 'PekerjaanController@store');
Route::get('pekerjaan/{id}', 'PekerjaanController@detail');
Route::get('pekerjaan/{id}/delete', 'PekerjaanController@delete');
Route::post('pekerjaan/{id}/update', 'PekerjaanController@update');
Route::get('pekerjaan/{id}/edit', 'PekerjaanController@edit');